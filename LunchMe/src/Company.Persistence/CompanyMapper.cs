namespace Company.Persistence
{
    using Domain;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CompanyMapper : IEntityTypeConfiguration<CompanyModel>
    {
        private const string TableName = "Company";
        private const int MaxSize = 1500;
        public void Configure(EntityTypeBuilder<CompanyModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.CompanyID);
            builder.Property(x => x.Name).HasMaxLength(MaxSize).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(MaxSize);
        }
    }
}