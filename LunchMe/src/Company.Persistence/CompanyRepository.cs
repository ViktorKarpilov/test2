namespace Company.Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Domain;
    using Microsoft.EntityFrameworkCore;
    using Company.Domain.Exceptions;

    public class CompanyRepository : ICompanyRepository
    {
        private readonly ApplicationContext _applicationContext;
        private const string CompanyNotFoundMessage = "Company with id = {0} is not found";

        public CompanyRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<IReadOnlyCollection<CompanyModel>> GetAllAsync()
        {
            return await _applicationContext.CompanyModels.ToListAsync();
        }

        public async Task<CompanyModel> GetAsync(Guid id)
        {
            var companyModel = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.CompanyID == id);
            if (companyModel == null)
            {
                throw new CompanyNotFoundDomainException(string.Format(CompanyNotFoundMessage, id));
            }
            return companyModel;
        }

        public async Task<CompanyModel> CreateAsync(CompanyModel companyModel)
        {
            _applicationContext.CompanyModels.Add(companyModel);
            await _applicationContext.SaveChangesAsync();
            return companyModel;
        }

        public async Task<CompanyModel> UpdateAsync(Guid id, CompanyModel companyModel)
        {
            CompanyModel modelToUpdate = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.CompanyID == id);
            if (modelToUpdate == null)
            {
                throw new CompanyNotFoundDomainException(string.Format(CompanyNotFoundMessage, id));
            }
            modelToUpdate.Name = companyModel.Name;
            modelToUpdate.Description = companyModel.Description;
            await _applicationContext.SaveChangesAsync();
            return modelToUpdate;
        }

        public async Task<CompanyModel> DeleteAsync(Guid id)
        {
            CompanyModel modelToRemove = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.CompanyID == id);
            if (modelToRemove == null)
            {
                throw new CompanyNotFoundDomainException(string.Format(CompanyNotFoundMessage, id));
            }
            _applicationContext.CompanyModels.Remove(modelToRemove);
            await _applicationContext.SaveChangesAsync();
            return modelToRemove;
        }
    }
}