namespace Company.Persistence
{
    using Domain;
    using Microsoft.EntityFrameworkCore;

    public class ApplicationContext : DbContext
    {
        public DbSet<CompanyModel> CompanyModels { get; set; }
        
        public ApplicationContext (DbContextOptions options) : base(options){}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<CompanyModel>(new CompanyMapper());
            base.OnModelCreating(modelBuilder);
        }
    }
}