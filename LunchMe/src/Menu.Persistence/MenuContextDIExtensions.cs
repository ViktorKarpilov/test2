﻿using Menu.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Menu.Persistence
{
    public static class MenuContextDIExtensions
    {
        private const string DBConnectionString = nameof(DBConnectionString);
        public static void RegisterMenuPersistence(this IServiceCollection collection, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString(DBConnectionString);
            collection.AddDbContextPool<MenuContext>(builder => builder.UseMySql(connection));
            collection.AddScoped<IMenuRepository, MenuRepository>();
            collection.AddScoped<IDishRepository, DishRepository>();
        }

    }
}
