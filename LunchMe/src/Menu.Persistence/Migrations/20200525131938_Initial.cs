﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Menu.Persistence.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DishComposition",
                table: "Dish",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price_Amount",
                table: "Dish",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Price_Currency",
                table: "Dish",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DishComposition",
                table: "Dish");

            migrationBuilder.DropColumn(
                name: "Price_Amount",
                table: "Dish");

            migrationBuilder.DropColumn(
                name: "Price_Currency",
                table: "Dish");
        }
    }
}
