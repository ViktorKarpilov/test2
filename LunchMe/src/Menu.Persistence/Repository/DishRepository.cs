﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Menu.Domain;
using Microsoft.EntityFrameworkCore;

namespace Menu.Persistence
{
    public class DishRepository : IDishRepository
    {
        private readonly MenuContext _menuContext;
        const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        const string DeleteMessage = "Couldn't be deleted.";
        const string CreateMessage = "Couldn't be created. Please check sending data.";
        const string DishNotFoundMessage = "Dish with id = {0} is not found";
        public DishRepository(MenuContext menuContext)
        {
            _menuContext = menuContext;
        }  

        public async Task<DishModel> CreateAsync(DishModel dishModel)
        {
            try
            {
                _menuContext.DishModels.Add(dishModel);
                await _menuContext.SaveChangesAsync();
                return dishModel;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(CreateMessage);
            }
        }
        public async Task DeleteAsync(Guid dishId)
        {
            DishModel modelToDelete = await _menuContext.DishModels.SingleAsync(model => model.Id == dishId);
            if (modelToDelete == null)
            {
                throw new NotFoundMenuDomainException(String.Format(DishNotFoundMessage, dishId));
            }
            try
            {
                _menuContext.DishModels.Remove(modelToDelete);
                await _menuContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(DeleteMessage);
            }
        }
        public async Task<IReadOnlyCollection<DishModel>> GetAllAsync()
        {
            return await _menuContext.DishModels.ToListAsync();
        }
        public async Task<DishModel> GetByIdAsync(Guid dishId)
        {
            var res = await _menuContext.DishModels.SingleAsync(dishModel => dishModel.Id == dishId);
            if (res == null)
            {
                throw new NotFoundMenuDomainException(String.Format(DishNotFoundMessage, dishId));
            }
            return res;
        }

        public async Task<DishModel> UpdateAsync(Guid dishId, DishModel dishModel)
        {
            DishModel modelToUpdate = await _menuContext.DishModels.SingleAsync(model => model.Id == dishId);
            if (modelToUpdate == null)
            {
                throw new NotFoundMenuDomainException(String.Format(DishNotFoundMessage, dishId));
            }
            modelToUpdate.PreparationTime = dishModel.PreparationTime;
            modelToUpdate.SetPrice(dishModel.Price.Amount, dishModel.Price.Currency);
            modelToUpdate.SetNutritionalValue(dishModel.DishPFC.CaloricContent,dishModel.DishPFC.Carbohydrates,dishModel.DishPFC.Fats, dishModel.DishPFC.Proteins);
            modelToUpdate.DishName = dishModel.DishName;
            modelToUpdate.DishComposition = dishModel.DishComposition;
            modelToUpdate.Category = dishModel.Category;
            try
            {
                _menuContext.DishModels.Update(modelToUpdate);
                await _menuContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(UpdateMessage);
            }
        }
     
    }
}
