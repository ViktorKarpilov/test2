﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Menu.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Menu.Persistence
{
    public class MenuRepository :IMenuRepository
    {
        private readonly MenuContext _menuContext;
        const string MenuNotFoundMessage = "Menu with id = {0} is not found";
        const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        const string DeleteMessage = "Couldn't be deleted.";

        public MenuRepository(MenuContext applicationContext)
        {
            _menuContext = applicationContext;
        }
        public async Task<MenuModel> CreateAsync(MenuModel menuModel)
        {
            _menuContext.MenuModels.Add(menuModel);
            await _menuContext.SaveChangesAsync();
            return menuModel;
        }

        public async Task<IReadOnlyCollection<MenuModel>> GetAllAsync()
        {
            var menus = await _menuContext.MenuModels.Include(m => m.DishList).ToListAsync();
            return menus;
        }

        public async Task<MenuModel> GetbyIdAsync(Guid menuId)
        {
            MenuModel res = await _menuContext.MenuModels.Include(m => m.DishList).FirstOrDefaultAsync(menuModel => menuModel.Id == menuId);
            if (res == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            return res;
        }

        public async Task<IReadOnlyCollection<MenuModel>> GetByProviderCompanyIdAsync(Guid providerCompamnyId)
        {
            return await _menuContext.MenuModels.Include(m => m.DishList).Where(menuModel => menuModel.ProviderCompanyId == providerCompamnyId).ToListAsync();
        }

        public async Task<MenuModel> UpdateAsync(Guid menuId, MenuModel menuModel)
        {
            MenuModel modelToUpdate = await _menuContext.MenuModels.Include(m => m.DishList).FirstOrDefaultAsync(model => model.Id == menuId);
            if (modelToUpdate == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            modelToUpdate.MenuInfo = menuModel.MenuInfo;
            modelToUpdate.MenuName = menuModel.MenuName;
            modelToUpdate.ProviderCompanyId = menuModel.ProviderCompanyId;
            try
            {
                _menuContext.MenuModels.Update(modelToUpdate);
                await _menuContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (Exception)
            {
                throw new InvalidDataMenuDomainException(UpdateMessage);
            }
        }
        public async Task<IReadOnlyCollection<DishModel>> GetDishesByMenuIdAsync(Guid menuId)
        {
            MenuModel menuModel = await _menuContext.MenuModels.Include(m => m.DishList).FirstOrDefaultAsync(model => model.Id == menuId);
            if (menuModel == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            return menuModel.DishList;
        }
        public async Task DeleteAsync(Guid menuId)
        {
            MenuModel modelToDelete = await _menuContext.MenuModels.Include(m => m.DishList).FirstOrDefaultAsync(model => model.Id == menuId);
            if (modelToDelete == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }

            try
            {
                _menuContext.MenuModels.Remove(modelToDelete);
                await _menuContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(DeleteMessage);
            }
        }

    }
}
