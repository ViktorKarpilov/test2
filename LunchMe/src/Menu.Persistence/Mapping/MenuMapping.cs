﻿using System;
using Menu.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Menu.Persistence
{
    public class MenuMapping : IEntityTypeConfiguration<MenuModel>
    {
        private const string TableName = "Menu";
        private const int MaxSize = 1500;
        public void Configure(EntityTypeBuilder<MenuModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.MenuName)
                .HasMaxLength(MaxSize);
            builder.Property(x => x.MenuInfo)
                .HasMaxLength(MaxSize);
            builder.Property(x => x.ProviderCompanyId);
        }
    }
}
