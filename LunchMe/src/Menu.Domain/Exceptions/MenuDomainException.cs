﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Menu.Domain
{
    public class MenuDomainException : Exception
    {
        public MenuDomainException() : base() { }

        public MenuDomainException(string message) : base(message) { }

        public MenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
    public class NotFoundMenuDomainException : MenuDomainException
    {
        public NotFoundMenuDomainException() : base() { }

        public NotFoundMenuDomainException(string message) : base(message) { }

        public NotFoundMenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
    public class InvalidDataMenuDomainException : MenuDomainException
    {
        public InvalidDataMenuDomainException() : base() { }

        public InvalidDataMenuDomainException(string message) : base(message) { }

        public InvalidDataMenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
