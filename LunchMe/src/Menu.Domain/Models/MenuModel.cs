﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Menu.Domain
{
    public class MenuModel : Entity
    { 
        public string MenuName { get; set; }
        public string MenuInfo { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public List<DishModel> DishList { get; set; }

        private MenuModel()
        {
        }
        public MenuModel(string menuName, string menuInfo, Guid providerCompanyId)
        {
            try
            {
                MenuName = menuName;
                MenuInfo = menuInfo;
                ProviderCompanyId = providerCompanyId;
                DishList = new List<DishModel>();
            }
            catch (FormatException ex)
            {
                throw new InvalidDataMenuDomainException(ex.Message);
            }
        }
        public TimeSpan GetMenuPreparationTime()
        {
            TimeSpan preparationTime;
            if (DishList != null && DishList.Count>0)
            {
                preparationTime = DishList.Max(dish => dish.PreparationTime);
            }
            else preparationTime = TimeSpan.Zero;
            return preparationTime;
        }
     

    }
}
