﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Menu.Domain
{
    public class DishModel: Entity
    {
        public string DishName { get; set; }
        public string Category { get; set; }
        public List<string> DishComposition { get; set; }
        public TimeSpan PreparationTime { get; set; }
        public NutritionalValue DishPFC { get; private set; }
        public Price Price { get; private set; }
        public Guid MenuID { get; set; }
        public MenuModel Menu { get; set; }

        public DishModel(string dishName, string category, TimeSpan preparationTime, List<string> composition, Guid menuId)
        {
            try
            {
                DishName = dishName;
                Category = category;
                DishComposition = composition;
                PreparationTime = preparationTime;
                MenuID = menuId;
            }
            catch (FormatException ex)
            {
                throw new InvalidDataMenuDomainException(ex.Message);
            }
        }

        private DishModel() {}
        public void SetPrice(decimal amount, string currency)
        {
            if (amount < 0)
            {
                throw new InvalidDataMenuDomainException("Price couldn't be negative");
            }
            Price = new Price(amount, currency);
        }
         public void SetNutritionalValue (double caloricContent, double carbohydrates, double fats, double proteins)
        {
            if (carbohydrates < 0 || caloricContent < 0 || fats < 0 || proteins < 0)
            {
                throw new InvalidDataMenuDomainException("Values of nutritional value couldn't be negative");
            }
            DishPFC = new NutritionalValue(proteins, fats,carbohydrates, caloricContent);
        }
    }
}
