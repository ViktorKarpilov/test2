﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public class MenuDomainService : IMenuDomainService
    {
        private readonly IMenuRepository _menuRepository;
        public MenuDomainService(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }
        public Task<MenuModel> CreateAsync(MenuModel menuModel)
        {
            return _menuRepository.CreateAsync(menuModel);
        }

        public Task<IReadOnlyCollection<MenuModel>> GetAllAsync()
        {
            return _menuRepository.GetAllAsync();
        }

        public Task<MenuModel> GetbyIdAsync(Guid menuId)
        {
            return _menuRepository.GetbyIdAsync(menuId);
        }
        public Task<IReadOnlyCollection<DishModel>> GetDishesByMenuIdAsync(Guid menuId)
        {
            return _menuRepository.GetDishesByMenuIdAsync(menuId);
        }
        public Task<IReadOnlyCollection<MenuModel>> GetByProviderCompanyIdAsync(Guid providerCompamnyId)
        {
            return _menuRepository.GetByProviderCompanyIdAsync(providerCompamnyId);
        }

        public Task<MenuModel> UpdateAsync(Guid menuId,MenuModel menuModel)
        {
            return _menuRepository.UpdateAsync(menuId,menuModel);
        }

        public Task DeleteAsync(Guid menuId)
        {
            return _menuRepository.DeleteAsync(menuId);
        }
    }
}
