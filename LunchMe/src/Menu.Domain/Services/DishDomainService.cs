﻿using Menu.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Domain.Services
{
    public class DishDomainService : IDishDomainService
    {
        private readonly IDishRepository _dishRepository;
        public DishDomainService(IDishRepository dishRepository)
        {
            _dishRepository = dishRepository;
        }

      
        public  Task<DishModel> CreateAsync(DishModel dishModel)
        {
            return  _dishRepository.CreateAsync(dishModel);
        }

        public Task DeleteAsync(Guid dishId)
        {
            return _dishRepository.DeleteAsync(dishId);
        }

        public Task<IReadOnlyCollection<DishModel>> GetAllAsync()
        {
            return _dishRepository.GetAllAsync();
        }

        public Task<DishModel> GetByIdAsync(Guid dishId)
        {
            return _dishRepository.GetByIdAsync(dishId);
        }

        public Task<DishModel> UpdateAsync(Guid dishId, DishModel dishModel)
        {
            return _dishRepository.UpdateAsync(dishId,dishModel);
        }
    }
}
