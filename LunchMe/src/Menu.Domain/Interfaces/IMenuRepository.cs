﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public interface IMenuRepository
    {
        Task<MenuModel> CreateAsync(MenuModel menuModel);
        Task<IReadOnlyCollection<MenuModel>> GetAllAsync();
        Task<IReadOnlyCollection<MenuModel>> GetByProviderCompanyIdAsync(Guid providerCompamnyId);
        Task<MenuModel> GetbyIdAsync(Guid menuId);
        Task<IReadOnlyCollection<DishModel>> GetDishesByMenuIdAsync(Guid menuId);
        Task<MenuModel> UpdateAsync(Guid menuId,MenuModel menuModel);
        Task DeleteAsync(Guid menuId);
    }
}
