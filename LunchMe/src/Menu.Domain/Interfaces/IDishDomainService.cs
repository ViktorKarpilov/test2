﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public interface IDishDomainService
    {
        Task<DishModel> CreateAsync(DishModel dishModel);
        Task<IReadOnlyCollection<DishModel>> GetAllAsync();
        Task<DishModel> UpdateAsync(Guid dishId,DishModel dishModel);
        Task DeleteAsync(Guid dishId);
        Task<DishModel> GetByIdAsync(Guid dishId);
    }
}
