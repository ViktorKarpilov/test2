﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public interface IDishRepository
    {
        Task<DishModel> CreateAsync(DishModel dishModel);
        Task DeleteAsync(Guid dishId);
        Task<IReadOnlyCollection<DishModel>> GetAllAsync();
        Task<DishModel> GetByIdAsync(Guid dishId);
        Task<DishModel> UpdateAsync(Guid dishId, DishModel dishModel);
    }
}
