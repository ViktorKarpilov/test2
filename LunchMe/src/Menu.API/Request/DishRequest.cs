﻿using Menu.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Menu.API
{
    public class DishRequest
    {
        public Guid DishId { get; set; }
        public string DishName { get; set; }
        public string Category { get; set; }
        public List<string> DishComposition { get; set; }
        public string PreparationTime { get; set; }
        public double Protein { get; set; }
        public double Fats { get; set; }
        public double Carbohydrates { get; set; }
        public double CaloricContent { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public Guid MenuID { get; set; }

    }
}
