﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Menu.API
{
    public class MenuRequest
    {
        public string MenuName { get; set; }
        public string MenuInfo { get; set; }
        public Guid ProviderCompanyId { get; set; }
    }
}
