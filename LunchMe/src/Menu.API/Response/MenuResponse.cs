﻿using System;
using System.Collections.Generic;
using System.Text;
using Menu.Domain;
using static Menu.API.MenuMappers;

namespace Menu.API
{
    public class MenuResponse
    {
        public Guid Id { get; set; }
        public string MenuName { get; set; }
        public string MenuInfo { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public List<DishResponse> DishList { get; set; }
        public TimeSpan PreparationTime { get; set; }
        
        public MenuResponse(Guid menuId, string menuName, string menuInfo, Guid providerCompanyId, List<DishModel> dishModels, TimeSpan preparationTime)
        {
            Id = menuId;
            MenuName = menuName;
            MenuInfo = menuInfo;
            ProviderCompanyId = providerCompanyId;
            DishList = new List<DishResponse>();
            if (dishModels != null)
            {
                foreach (DishModel dish in dishModels)
                {
                    DishResponse dishResponse = dish.MapTo();
                    DishList.Add(dishResponse);
                }
            }
            PreparationTime = preparationTime;
        }

    }
}
