﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Menu.API
{
    [ApiController]
    [Route("[controller]")]
    public class MenuController: ControllerBase
    {
        private readonly ILogger<MenuController> _logger;
        private readonly IMenuDomainService _menuDomainService;

        public MenuController(ILogger<MenuController> logger, IMenuDomainService menuDomainService)
        {
            _logger = logger;
            _menuDomainService = menuDomainService;
        }

        /// <summary>
        /// Get all menues
        /// </summary>
        /// <returns>List of menues with dishes and menues information</returns>
        [HttpGet]
        public async Task<ActionResult<IReadOnlyCollection<MenuResponse>>> GetAll()
        {
            try
            {
                IReadOnlyCollection<MenuModel> menuModels = await _menuDomainService.GetAllAsync();
                return menuModels.Select(m => m.MapTo()).ToList();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
     
        /// <summary>
        /// Get menu by menue id
        /// </summary>
        /// <param name="id">Unique menu id</param>
        /// <returns>Menu with information and dishes</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MenuResponse>> Get(Guid id)
        {
            try
            {
                MenuModel res = await _menuDomainService.GetbyIdAsync(id);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        
        /// <summary>
        /// Get dishes by menu id
        /// </summary>
        /// <param name="menuId">Id of menu, which contain dishes</param>
        /// <returns>List of dishes with information</returns>
        [HttpGet("{id}/dishes")]
        public async Task<ActionResult<IReadOnlyCollection<DishResponse>>> GetByMenu(Guid id)
        {
            try
            {
                IReadOnlyCollection<DishModel> dishModels = await _menuDomainService.GetDishesByMenuIdAsync(id);
                return dishModels.Select(d => d.MapTo()).ToList();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get menues by provider companies id
        /// </summary>
        /// <param name="providerCompanyid">Unique provider company id</param>
        /// <returns>Collection of menues with information and dishes</returns>
        [HttpGet("provider/{providerCompanyid}")]
        public async Task<ActionResult<IReadOnlyCollection<MenuResponse>>> GetByProvider(Guid providerCompanyid)
        {
            try
            {
                IReadOnlyCollection<MenuModel> menuModels = await _menuDomainService.GetByProviderCompanyIdAsync(providerCompanyid);
                return menuModels.Select(m => m.MapTo()).ToList();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Create new menu
        /// </summary>
        /// <param name="request">Menue information from UI</param>
        /// <returns>Menu, which was created</returns>
        [HttpPost]
        public async Task<ActionResult<MenuResponse>> CreateMenu([FromBody] MenuRequest request)
        {
            try
            {
                var model = request.MapFrom();
                var res = await _menuDomainService.CreateAsync(model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Update existing menu
        /// </summary>
        /// <param name="menuId"> Unique menu id</param>
        /// <param name="request"> Uodating information from UI</param>
        /// <returns>Menu, which was updated</returns>
        [HttpPut]
        public async Task<ActionResult<MenuResponse>> UpdateMenu(Guid menuId,[FromBody] MenuRequest request)
        {
            try
            {
                MenuModel model = request.MapFrom();
                MenuModel res = await _menuDomainService.UpdateAsync(menuId, model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        
        /// <summary>
        /// Delete menu with dishes by menu id
        /// </summary>
        /// <param name="id">Unique menu id</param>
        /// <returns>Action status</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await _menuDomainService.DeleteAsync(id);
                return Ok();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


    }
}
