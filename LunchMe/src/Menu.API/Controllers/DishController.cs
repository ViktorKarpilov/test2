﻿using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DishController : ControllerBase
    {
        private readonly ILogger<DishController> _logger;
        private readonly IDishDomainService _dishDomainService;

        public DishController(ILogger<DishController> logger, IDishDomainService dishDomainService)
        {
            _logger = logger;
            _dishDomainService = dishDomainService;
        }

        /// <summary>
        /// Get all dishes 
        /// </summary>
        /// <returns>Collection of dishes with name of dish, category, information about price, PFC, preparation time, menu`s id </returns>
        [HttpGet]
        public async Task<ActionResult<IReadOnlyCollection<DishResponse>>> GetAll()
        {
            try
            {
                IReadOnlyCollection<DishModel> dishModels = await _dishDomainService.GetAllAsync();
                return dishModels.Select(d => d.MapTo()).ToList();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get dish by dish id
        /// </summary>
        /// <param name="id"> The unique dish id </param>
        /// <returns>Dish with name, category, information about price, PFC, preparation time, menu`s id,</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<DishResponse>> Get(Guid id)
        {
            try
            {
                DishModel res = await _dishDomainService.GetByIdAsync(id);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Create new dish 
        /// </summary>
        /// <param name="request">Dish information for creation</param>
        /// <returns>Dish, which was created</returns>
        [HttpPost]
        public async Task<ActionResult<DishResponse>> CreateDish([FromBody] DishRequest request)
        {
            try
            {
                var model = request.MapFrom();
                var res = await _dishDomainService.CreateAsync(model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Update exsting dish
        /// </summary>
        /// <param name="dishId">Unique dish id</param>
        /// <param name="request">Information of dish, which should update</param>
        /// <returns>Updated dish information</returns>
        [HttpPut]
        public async Task<ActionResult<DishResponse>> UpdateDish(Guid dishId,[FromBody] DishRequest request)
        {
            try
            {
                DishModel model = request.MapFrom();
                DishModel res = await _dishDomainService.UpdateAsync(dishId, model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Delete dish by id
        /// </summary>
        /// <param name="id">Unique dish id</param>
        /// <returns>Action status</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await _dishDomainService.DeleteAsync(id);
                return Ok();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
