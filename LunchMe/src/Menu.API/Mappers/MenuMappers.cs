﻿using Menu.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Menu.API
{
    public static class MenuMappers
    {
        public static MenuModel MapFrom(this MenuRequest request)
        {
            return new MenuModel(request.MenuName, request.MenuInfo, request.ProviderCompanyId);
        }
            

        public static MenuResponse MapTo(this MenuModel menuModel)
        {
            TimeSpan preparationTime = menuModel.GetMenuPreparationTime();
            return new MenuResponse(menuModel.Id, menuModel.MenuName, menuModel.MenuInfo, menuModel.ProviderCompanyId, menuModel.DishList, preparationTime);
        }

        public static DishResponse MapTo(this DishModel dishModel)
        {

            return new DishResponse()
            {
                Id = dishModel.Id,
                DishName = dishModel.DishName,
                DishComposition = dishModel.DishComposition,
                PreparationTime = dishModel.PreparationTime,
                Category = dishModel.Category,
                MenuID = dishModel.MenuID,
                Protein = dishModel.DishPFC.Proteins,
                Fats = dishModel.DishPFC.Fats,
                Carbohydrates = dishModel.DishPFC.Carbohydrates,
                CaloricContent = dishModel.DishPFC.CaloricContent,
                Currency = dishModel.Price.Currency,
                Amount = dishModel.Price.Amount

            };
        }

        public static DishModel MapFrom(this DishRequest request)
        {
            try
            {
                DishModel dishModel = new DishModel(request.DishName, request.Category, TimeSpan.Parse(request.PreparationTime), request.DishComposition, request.MenuID);
                dishModel.SetNutritionalValue(request.CaloricContent, request.Carbohydrates, request.Fats, request.Protein);
                dishModel.SetPrice(request.Amount, request.Currency);
                return dishModel;
            }
            catch (MenuDomainException)
            {
                throw;
            }
        }
    }
}

