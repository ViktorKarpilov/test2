﻿using System;

namespace Company.Domain.Exceptions
{
    public class CompanyNotFoundDomainException : Exception
    {
        public CompanyNotFoundDomainException() : base() { }

        public CompanyNotFoundDomainException(string message) : base(message) { }
    }
}
