namespace Company.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class CompanyDomainService : ICompanyDomainServices
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyDomainService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public Task<IReadOnlyCollection<CompanyModel>> GetAllAsync()
        {
            return _companyRepository.GetAllAsync();
        }
        public Task<CompanyModel> GetAsync(Guid id)
        {
            return _companyRepository.GetAsync(id);
        }

        public Task<CompanyModel> CreateAsync(CompanyModel companyModel)
        {
            return _companyRepository.CreateAsync(companyModel);
        }

        public Task<CompanyModel> UpdateAsync(Guid id, CompanyModel companyModel)
        {
            return _companyRepository.UpdateAsync(id, companyModel);
        }

        public Task<CompanyModel> DeleteAsync(Guid id)
        {
            return _companyRepository.DeleteAsync(id);
        }
    }
}