namespace Company.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICompanyRepository
    {
        Task<IReadOnlyCollection<CompanyModel>> GetAllAsync();
        Task<CompanyModel> GetAsync(Guid id);
        Task<CompanyModel> CreateAsync(CompanyModel companyModel);
        Task<CompanyModel> UpdateAsync(Guid id, CompanyModel companyModel);
        Task<CompanyModel> DeleteAsync(Guid id);
    }
}