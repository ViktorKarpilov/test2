﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain
{
    using Microsoft.Extensions.DependencyInjection;

    public static class CompanyDomainExtensions
    {
        public static void RegisterCompanyDomain(this IServiceCollection collection)
        {
            collection.AddScoped<ICompanyDomainServices, CompanyDomainService>();
        }
    }
}
