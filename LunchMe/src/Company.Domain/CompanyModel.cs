﻿using System;

namespace Company.Domain
{
    public class CompanyModel
    {
        public Guid CompanyID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
