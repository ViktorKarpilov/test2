﻿using Company.API.Request;
using Company.API.Response;
using Company.Domain;

namespace Company.API.Mappers
{
    public static class CompanyMappers
    {
        public static CompanyModel MapFromRequest(this CompanyRequest request) 
            => new CompanyModel
            {
                Name = request.Name,
                Description = request.Description
            };

        public static CompanyResponse MapToResponse(this CompanyModel model)
            => new CompanyResponse
            {
                CompanyID = model.CompanyID,
                Name = model.Name,
                Description = model.Description
            };
    }
}
