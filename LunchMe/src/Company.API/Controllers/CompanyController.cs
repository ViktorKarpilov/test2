﻿using Company.API.Request;
using Company.API.Response;
using static Company.API.Mappers.CompanyMappers;
using Company.Domain;
using Company.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.API.Validation;

namespace Company.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyDomainServices _companyDomainService;

        private const string validationFailedMessage = "Company validation failed: name is empty or strings is too long";

        public CompanyController(ICompanyDomainServices companyDomainService)
        {
            _companyDomainService = companyDomainService;
        }

        [HttpGet]
        public async Task<IReadOnlyCollection<CompanyResponse>> GetAllCompanies()
        {
            IReadOnlyCollection<CompanyModel> companyModels = await _companyDomainService.GetAllAsync();

            return companyModels.Select(cm => cm.MapToResponse())
                                        .ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CompanyResponse>> GetCompany(Guid id)
        {
            try
            {
                CompanyModel res = await _companyDomainService.GetAsync(id);
                return res.MapToResponse();
            }
            catch (CompanyNotFoundDomainException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<CompanyResponse>> CreateCompany([FromBody] CompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(validationFailedMessage);
            }
            CompanyModel model = request.MapFromRequest();
            CompanyModel res = await _companyDomainService.CreateAsync(model);
            return res.MapToResponse();
        }

        [HttpPut]
        public async Task<ActionResult<CompanyResponse>> UpdateCompany(Guid id, [FromBody] CompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(validationFailedMessage);
            }
            CompanyModel model = request.MapFromRequest();
            try
            {
                CompanyModel res = await _companyDomainService.UpdateAsync(id, model);
                return res.MapToResponse();
            }
            catch (CompanyNotFoundDomainException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public async Task<ActionResult<CompanyResponse>> RemoveCompany(Guid id)
        {
            try
            {
                CompanyModel res = await _companyDomainService.DeleteAsync(id);
                return res.MapToResponse();
            }
            catch (CompanyNotFoundDomainException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
