﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.API.Request
{
    public class CompanyRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
