﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.API.Response
{
    public class CompanyResponse
    {
        public Guid CompanyID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
