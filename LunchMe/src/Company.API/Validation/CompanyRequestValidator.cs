﻿using Company.API.Request;
using FluentValidation;

namespace Company.API.Validation
{
    public class CompanyRequestValidator : AbstractValidator<CompanyRequest>
    {
        const int stringMinSize = 1;
        const int stringMaxSize = 1500;
        public CompanyRequestValidator()
        {
            RuleFor(c => c.Name).NotNull().NotEmpty();
            RuleFor(c => c.Name).Length(stringMinSize, stringMaxSize);
            RuleFor(c => c.Description).MaximumLength(stringMaxSize);
        }
    }
}
