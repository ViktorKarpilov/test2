﻿using Menu.Domain;
using Menu.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MenuServiceTests
{
    public class DishDomainServiceTests
    {
        private readonly DishDomainService _dishDomainService;
        private readonly Mock<IDishRepository> _dishRepoMock = new Mock<IDishRepository>();

        public DishDomainServiceTests()
        {
            _dishDomainService = new DishDomainService(_dishRepoMock.Object);
        }



        [Fact]
        public async Task GetByIdAsync_ShouldReturnDish_WhenDishExists()
        {
            //Arrange
            var dishId = Guid.NewGuid();

            DishModel dishTest = new DishModel("some cake", "category", TimeSpan.FromMinutes(25), new List<string>(), Guid.NewGuid());
            dishTest.Id = dishId;
            dishTest.Category = "cakes";
            _dishRepoMock.Setup(x => x.GetByIdAsync(dishId)).ReturnsAsync(dishTest);

            //Act     
            var dish = await _dishDomainService.GetByIdAsync(dishId);

            //Assert
            Assert.Equal(dishId, dish.Id);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnNothing_WhenDishDoesNotExist()
        {
            // Arrange
            _dishRepoMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            // Act
            var menu = await _dishDomainService.GetByIdAsync(Guid.NewGuid());

            // Assert
            Assert.Null(menu);
        }

        [Fact]
        public async Task CreateAsync_ShouldReturnNewDishModel_Test()
        {
            Guid testId = Guid.NewGuid();
            var dishMenu = new DishModel("some cake", "category", TimeSpan.FromMinutes(25), new List<string>(), Guid.NewGuid());
            dishMenu.Id = testId;
            _dishRepoMock.Setup(x => x.CreateAsync(dishMenu))
                .ReturnsAsync(() =>
                {

                    return dishMenu;
                });
            var dish = await _dishDomainService.CreateAsync(dishMenu);

            var result = Assert.IsType<DishModel>(dish);
            Assert.Equal(testId, result.Id);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async void CreateAsync_ShouldThrowException_WhenNameIsNullOrEmpty(string name)
        {
            //Arrange
            var service = new DishDomainService(null);
            Guid testId = Guid.NewGuid();

            //Act
            var point = new DishModel("some cake", name, TimeSpan.FromMinutes(25), new List<string>(), Guid.NewGuid());
            point.Id = testId;

            //Assert
            await Assert.ThrowsAsync<NullReferenceException>(() => service.CreateAsync(point));
        }


        [Fact]
        public async void UpdateAsync_ShouldReturnSameInfo_AfterUpdate()
        {
            Guid testId = Guid.NewGuid();
            var dishMenu = new DishModel("some cake", "category", TimeSpan.FromMinutes(25), new List<string>(), Guid.NewGuid());
            _dishRepoMock.Setup(x => x.UpdateAsync(testId, It.IsAny<DishModel>()))
                .ReturnsAsync(() =>
                {
                    dishMenu.Id = testId;
                    return dishMenu;
                });

            var check = await _dishDomainService.UpdateAsync(testId, new DishModel("some cake", "category", TimeSpan.FromMinutes(25), new List<string>(), Guid.NewGuid()));
            var result = Assert.IsType<DishModel>(check);
            Assert.Equal(testId, result.Id);
            Assert.Equal(dishMenu.DishName, result.DishName);
            Assert.Equal(dishMenu.Category, result.Category);

        }
    }
}
