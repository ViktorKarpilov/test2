﻿using Menu.Domain;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MenuServiceTests
{
    public class MenuServiceTest
    {
        private readonly IMenuDomainService _menuDomainService;
        private readonly Mock<IMenuRepository> _menuRepoMock = new Mock<IMenuRepository>();

        public MenuServiceTest()
        {
            _menuDomainService = new MenuDomainService(_menuRepoMock.Object);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnMenu_WhenMenuExists()
        {
            //Arrange
            var menuId = Guid.NewGuid();
            MenuModel menuDTO = new MenuModel("HappyMeal", "hamburgers", Guid.NewGuid());
            menuDTO.Id = menuId;
            _menuRepoMock.Setup(x => x.GetbyIdAsync(menuId)).ReturnsAsync(menuDTO);

            //Act
            var menu = await _menuDomainService.GetbyIdAsync(menuId);

            //Assert
            Assert.Equal(menuId, menu.Id);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnNothing_WhenMenuDoesNotExist()
        {
            // Arrange
            _menuRepoMock.Setup(x => x.GetbyIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            // Act
            var menu = await _menuDomainService.GetbyIdAsync(Guid.NewGuid());

            // Assert
            Assert.Null(menu);
        }

        [Fact]
        public async Task GetByProviderCompanyIdAsync_ShouldReturnMenu_WhenMenuExists()// To Do Need to Check!
        {
            //Arrange
            var providerId = Guid.NewGuid();
            List<MenuModel> menulist = new List<MenuModel>();

            menulist.Add(new MenuModel("HappyMeal", "hamburgers", providerId));
            menulist.Add(new MenuModel("HappyMeal", "hamburgers", providerId));
            menulist.Add(new MenuModel("HappyMeal", "hamburgers", providerId));



            _menuRepoMock.Setup(x => x.GetByProviderCompanyIdAsync(providerId)).ReturnsAsync(menulist);

            //Act
            var menus = await _menuDomainService.GetByProviderCompanyIdAsync(providerId);

            //Assert
            Assert.NotEmpty(menus);
        }

        [Fact]
        public async Task GetByProviderCompanyIdAsync_ShouldReturnNothing_WhenMenuDoesNotExist()
        {
            // Arrange
            _menuRepoMock.Setup(x => x.GetByProviderCompanyIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            // Act
            var menu = await _menuDomainService.GetByProviderCompanyIdAsync(Guid.NewGuid());

            // Assert
            Assert.Null(menu);
        }
        [Fact]
        public async Task CreateAsync_ShouldReturnNewMenuModel_Test()
        {
            Guid testId = Guid.NewGuid();
            var testMenu = new MenuModel("Cakes", "some info about menu", Guid.NewGuid())
            {
                Id = testId,
            };

            _menuRepoMock.Setup(x => x.CreateAsync(testMenu))
                .ReturnsAsync(() =>
                {
                    //testMenu.Id = testId;
                    return testMenu;
                });
            var menu = await _menuDomainService.CreateAsync(testMenu);

            var result = Assert.IsType<MenuModel>(menu);
            Assert.Equal(testId, result.Id);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async void CreateAsync_ShouldThrowException_WhenNameIsNullOrEmpty(string name)
        {
            //Arrange
            var service = new MenuDomainService(null);
            Guid testId = Guid.NewGuid();

            //Act
            var point = new MenuModel(name, "some info about menu", Guid.NewGuid())
            {
                Id = testId,
            };

            //Assert
            await Assert.ThrowsAsync<NullReferenceException>(() => service.CreateAsync(point));
        }


        [Fact]
        public async void UpdateAsync_ShouldReturnSameInfo_AfterUpdate()
        {
            Guid testId = Guid.NewGuid();
            _menuRepoMock.Setup(x => x.UpdateAsync(testId, It.IsAny<MenuModel>()))
                .ReturnsAsync(() => new MenuModel("Cakes", "some info about menu", Guid.NewGuid())
                {
                    Id = testId,
                });
            var check = await _menuDomainService.UpdateAsync(testId, new MenuModel("Cakes", "some info about menu", Guid.NewGuid()));
            var result = Assert.IsType<MenuModel>(check);
            Assert.Equal(testId, result.Id);
            Assert.Equal(check.MenuName, result.MenuName);
            Assert.Equal(check.MenuInfo, result.MenuInfo);

        }

        [Fact]
        public async Task GetDishesByMenuIdAsync_ShouldReturnDish_WhenDishExists()
        {
            //Arrange
            var menuId = Guid.NewGuid();

            List<DishModel> dishes = new List<DishModel>();

            dishes.Add(new DishModel("some cake", "category", TimeSpan.FromMinutes(25), new List<string>(), menuId));
            dishes.Add(new DishModel("some meet", "category", TimeSpan.FromMinutes(30), new List<string>(), menuId));
            dishes.Add(new DishModel("some fish", "category", TimeSpan.FromMinutes(15), new List<string>(), menuId));


            _menuRepoMock.Setup(x => x.GetDishesByMenuIdAsync(menuId)).ReturnsAsync(dishes);

            //Act     
            var dishList = await _menuDomainService.GetDishesByMenuIdAsync(menuId);

            //Assert
            Assert.NotEmpty(dishList);
        }

        [Fact]
        public async Task GetDishesByMenuIdAsync_ShouldReturnNothing_WhenDishDoesNotExist()
        {
            // Arrange
            _menuRepoMock.Setup(x => x.GetDishesByMenuIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            // Act
            var menu = await _menuDomainService.GetDishesByMenuIdAsync(Guid.NewGuid());

            // Assert
            Assert.Null(menu);
        }
    }
    }
