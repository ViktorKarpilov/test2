﻿using Menu.API;
using Menu.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Menu.Controllers.Test.Mappers
{
    public class MenuMapperTest
    {
        [Theory,MemberData(nameof(TestCases))]
        public void MapFromRequestTest(string name, string menuinfo)
        {
            var providerGuidTest = Guid.NewGuid();
            // Arrange
            var request = new MenuRequest()
            {
                MenuName = name,
                MenuInfo = menuinfo,
                ProviderCompanyId = providerGuidTest
            };
            // Act
            var model = request.MapFrom();
            // Assert
            Assert.Equal(request.MenuName, model.MenuName);
            Assert.Equal(request.MenuInfo, model.MenuInfo);
        }

        [Theory, MemberData(nameof(TestCases))]
        public void MapToResponseTest(string name, string description)
        {
            // Arrange
            var model = new MenuModel(name, description, Guid.NewGuid());
            // Act
            var response = model.MapTo();
            // Assert

            Assert.Equal(model.MenuName, response.MenuName);
            Assert.Equal(model.MenuInfo, response.MenuInfo);
        }
        public static IEnumerable<object[]> TestCases
        {
            get
            {
                yield return new object[] { "Cake", "some cake" };
                yield return new object[] { "", "some cake" };
                yield return new object[] { "Cake", null };
            }
        }
    }
}
