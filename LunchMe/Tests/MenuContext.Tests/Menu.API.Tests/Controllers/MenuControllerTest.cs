﻿using Menu.API;
using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xunit;

namespace Menu.Controllers.Test
{
    public class MenuControllerTest
    {
        [Theory,MemberData(nameof(TestMenu))]
        public async void GetById_ExistingEntity_Test(string name,string category,Guid guid)
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();

            Guid requestedId = Guid.NewGuid();
            string testName = "Cakes";
            mockService.Setup(x => x.GetbyIdAsync(requestedId)).ReturnsAsync(
                new MenuModel(name, category, guid));
            var controller = new MenuController(null, mockService.Object);

            // Act
            var result = await controller.Get(requestedId);

            // Assert
            var response = Assert.IsType<MenuResponse>(result.Value);
            Assert.Equal(testName, response.MenuName);
        }
        public static IEnumerable<object[]> TestMenu
        {
            get
            {
                yield return new object[] { "Cakes", "Ordinary menu",Guid.NewGuid()};
            }
        }
        [Fact]
        public async void GetAll_ShouldReturn3Entities_test()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();
            mockService.Setup(ms => ms.GetAllAsync()).ReturnsAsync(GetTestModels());

            var controller = new MenuController(null, mockService.Object);

            // Act
            var result = await controller.GetAll();

            // Assert
            var list = result.Value;
            Assert.Equal(3, list.Count);
        }
        #region testinfo
        private IReadOnlyCollection<MenuModel> GetTestModels()
        {
            return new ReadOnlyCollection<MenuModel>(new List<MenuModel>
            {
                new MenuModel("Cakes", "Ordinary menu", Guid.NewGuid()) ,
                new MenuModel("Ice-cream", "Ordinary menu", Guid.NewGuid()) ,
                new MenuModel("Soup", "Ordinary menu", Guid.NewGuid())
            });
        }
        #endregion
        [Fact]
        public async void GetAllAsync_WhenListisEmpty_returns0()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(new List<MenuModel>());
            var controller = new MenuController(null, mockService.Object);

            // Act
            var result = await controller.GetAll();

            // Assert
            var list = result.Value;

            Assert.Equal(0,list.Count);
        }

        [Fact]
        public async void Create_ShouldReturnTrue_WhenResponseandTestStringAreEqual()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();
            Guid generatedId = Guid.NewGuid();
            Guid RequestProviderId = Guid.NewGuid();

            var modelToCreate = new MenuModel("", "cakes", Guid.NewGuid());

            var teststring = "test";
            mockService.Setup(x => x.CreateAsync(It.IsAny<MenuModel>()))
                .ReturnsAsync(() =>
                {
                    modelToCreate.MenuName = teststring;

                    return modelToCreate;
                });

            var controller = new MenuController(null, mockService.Object);

            // Act
            var request = new MenuRequest()
            {
                MenuName = "cakes",
                MenuInfo = "some info",
                ProviderCompanyId = RequestProviderId
            };
            var result = await controller.CreateMenu(request);

            // Assert
            var response = result.Value;
            Assert.Equal(teststring, response.MenuName);
        }

        

        [Fact]
        public async void Update_RequestNameShouldBeDifferent_From_BaseName()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<MenuModel>()))
                .ReturnsAsync(() => new MenuModel("cakes", "some info", Guid.NewGuid()));
            var controller = new MenuController(null, mockService.Object);

            // Act
            var request = new MenuRequest()
            {
                MenuName = "burgers",
                MenuInfo = "some info",
                ProviderCompanyId = requestedId
            };
            var result = await controller.UpdateMenu(requestedId, request);

            // Assert
            var response = result.Value;

            Assert.Equal(request.MenuInfo, response.MenuInfo);
            Assert.NotEqual(request.MenuName, response.MenuName);
        }
    }
}

