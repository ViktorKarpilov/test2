﻿using Menu.API;
using Menu.API.Controllers;
using Menu.Domain;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xunit;

namespace Menu.Controllers.Test
{
    public class DishControllerTest
    {
        NutritionalValue TestPFC = new NutritionalValue(1, 1, 1, 1);
        Price TestPrice = new Price(12, "USD");
        MenuModel Testmenu = new MenuModel("HappyMeal", "hamburgers", Guid.NewGuid());

        
        
        [Theory,MemberData(nameof(TestDish))]
        public async void GetById_ExistingEntity_Test(string name,string category,TimeSpan time,List<string> ingridients)
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();

            Guid requestedId = Guid.NewGuid();
            Guid MenuGuid = Guid.NewGuid();
            string testName = "Cakes";
            //List<string> dishComposition = new List<string>()
            //{
            //    "some ingredients"
            //};

            var modelToCreate = new DishModel(name, category,time, ingridients, MenuGuid);
            modelToCreate.SetPrice(TestPrice.Amount, TestPrice.Currency);
            modelToCreate.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);


            mockService.Setup(x => x.GetByIdAsync(requestedId)).ReturnsAsync(modelToCreate);

            var controller = new DishController(null, mockService.Object);

            // Act
            var result = await controller.Get(requestedId);

            // Assert
            var response = Assert.IsType<DishResponse>(result.Value);
            Assert.Equal(testName, response.DishName);
        }
        public static IEnumerable<object[]> TestDish
        {
            get
            {
                yield return new object[] { "Cakes", "category", TimeSpan.FromSeconds(1), new List<string>(){"some ingredients"} };
            }
        }

        [Fact]
        public async void GetAll_ShouldReturn_2_Items()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            mockService.Setup(ms => ms.GetAllAsync()).ReturnsAsync(GetTestModels());

            var controller = new DishController(null, mockService.Object);

            // Act
            var result = await controller.GetAll();

            // Assert
            var list = result.Value;
            Assert.Equal(2, list.Count);
        }
        #region testdata
        private IReadOnlyCollection<DishModel> GetTestModels()
        {
            var temp = Guid.NewGuid();
            List<string> dishComposition = new List<string>()
            {
                "some ingredients"
            };
            var FirstDishTest = new DishModel("Cakes", "category", TimeSpan.FromSeconds(1), dishComposition, temp);
            FirstDishTest.SetPrice(TestPrice.Amount, TestPrice.Currency);
            FirstDishTest.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);
            var SecondDishTest = new DishModel("Pizza", "Category", TimeSpan.FromSeconds(2), dishComposition, temp);
            SecondDishTest.SetPrice(TestPrice.Amount, TestPrice.Currency);
            SecondDishTest.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);
            List<DishModel> sample = new List<DishModel>();
            sample.Add(FirstDishTest);
            sample.Add(SecondDishTest);
            return new ReadOnlyCollection<DishModel>(sample);
        }
        #endregion

        [Fact]
        public async void Get_All_ReturnsNull_WhenEmpty()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(new List<DishModel>());
            var controller = new DishController(null, mockService.Object);

            // Act
            var result = await controller.GetAll();

            // Assert
            var list = result.Value;
            
            Assert.Equal(0,list.Count);
        }

        [Fact]
        public async void Create_ShouldPass_WhenResponseNameAndTestNameEqual()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            Guid generatedId = Guid.NewGuid();
            Guid MenuId = Guid.NewGuid();
            List<string> dishComposition = new List<string>()
            {
                "some ingredients"
            };

            var modelToCreate = new DishModel("Cakes", "category", TimeSpan.FromSeconds(1), dishComposition, MenuId);
            modelToCreate.SetPrice(TestPrice.Amount, TestPrice.Currency);
            modelToCreate.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);

            var testname = "test";
            mockService.Setup(x => x.CreateAsync(It.IsAny<DishModel>()))
                .ReturnsAsync(() =>
                {
                    modelToCreate.DishName = testname;

                    return modelToCreate;
                });

            var controller = new DishController(null, mockService.Object);

            // Act
            var request = new DishRequest()
            {
                DishId = generatedId,
                DishName = testname,
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };
            var result = await controller.CreateDish(request);

            // Assert
            var response = result.Value;
            Assert.Equal(testname, response.DishName);
        }



        [Fact]
        public async void Update_RequestNameShouldBeDifferent_From_BaseName()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            Guid requestedId = Guid.NewGuid();
            List<string> dishComposition = new List<string>()
            {
                "some ingredients"
            };

            var modelToCreate = new DishModel("Cakes", "category", TimeSpan.FromSeconds(1), dishComposition, requestedId);
            modelToCreate.SetPrice(TestPrice.Amount, TestPrice.Currency);
            modelToCreate.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);


            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<DishModel>()))
                .ReturnsAsync(modelToCreate);
            var controller = new DishController(null, mockService.Object);
            // Act
            var request = new DishRequest()
            {
                DishId = requestedId,
                DishName = "burgers",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "category",

                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1


            };

            var result = await controller.UpdateDish(requestedId, request);

            // Assert
            var response = result.Value;

            Assert.NotEqual(request.DishName, response.DishName);
            Assert.Equal(request.Category, response.Category);
        }
    }
}

