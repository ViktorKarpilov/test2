namespace Company.DomainServices.Tests
{
    using System;
    using System.Collections.Generic;
    using Domain;
    using Domain.Exceptions;
    using Moq;
    using Xunit;

    public class DomainServices_Tests
    {
        [Fact]
        public async void GetAllCompanies_WhenCollectionIsEmpty()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            mockRepository.Setup(n => n.GetAllAsync())
                .ReturnsAsync(new List<CompanyModel>());
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = await service.GetAllAsync();

            //Assert
            var result = Assert.IsType<List<CompanyModel>>(check);
            Assert.Empty(result);
        }

        [Fact]
        public async void GetCompaniesCount_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            var testModelsCollection = new List<CompanyModel>
            {
                new CompanyModel() {Name = "Apple", Description = "Think different"},
                new CompanyModel() {Name = "Lenovo", Description = "Different is better"},
                new CompanyModel() {Name = "Asus", Description = "In search of incredible"}
            };
            mockRepository
                .Setup(mR => mR.GetAllAsync()).ReturnsAsync(testModelsCollection);
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = await service.GetAllAsync();

            //Assert
            var result = Assert.IsType<List<CompanyModel>>(check);
            Assert.Equal(testModelsCollection.Count, result.Count);
        }

        [Fact]
        public async void GetSingleCompany_WhenItExists()
        {
            //Arrange
            var mockReppsitory = new Mock<ICompanyRepository>();
            Guid requestID = Guid.NewGuid();
            mockReppsitory.Setup(mR => mR.GetAsync(requestID))
                .ReturnsAsync(new CompanyModel()
                {
                    CompanyID = requestID, Name = "Asus", Description = "In search of incredible"
                });
            var service = new CompanyDomainService(mockReppsitory.Object);

            //Act
            var check = await service.GetAsync(requestID);

            //Assert
            var result = Assert.IsType<CompanyModel>(check);
            Assert.Equal(requestID, result.CompanyID);
        }

        [Fact]
        public async void GetSingleCompany_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository =  new Mock<ICompanyRepository>();
            Guid requestID = Guid.NewGuid();
            mockRepository
                .Setup(mR => mR.GetAsync(requestID)).Throws<CompanyNotFoundDomainException>();
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act & Assert
           await Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.GetAsync(requestID));
        }
        
        [Fact]
        public async void CreateValidCompany()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            var TestModel = new CompanyModel()
            {
                CompanyID = id, Name = "Asus", Description = "In search of incredible"
            };
            mockRepository.Setup(mR => mR.CreateAsync(TestModel))
                .ReturnsAsync(() =>
                {
                    TestModel.CompanyID = id;
                    return TestModel;
                });
            var service = mockRepository.Object;

            //Act
            var check = await service.CreateAsync(TestModel);

            //Assert
            var result = Assert.IsType<CompanyModel>(check);
            Assert.Equal(id, result.CompanyID);
        }

        [Fact]
        public async void UpdateCompany_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.UpdateAsync(id, It.IsAny<CompanyModel>()))
                .ReturnsAsync(() => new CompanyModel()
                {
                    CompanyID = id, Name = "Asus", Description = "In search of incredible"
                });
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = await service.UpdateAsync(id, new CompanyModel()
            {
                Name = "Asus", Description = "In search of incredible"
            });

            //Assert
            var result = Assert.IsType<CompanyModel>(check);
            Assert.Equal(id, result.CompanyID);
            Assert.Equal(check.Name, result.Name);
            Assert.Equal(check.Description, result.Description);
        }

        [Fact]
        public async void UpdateCompany_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository
                .Setup(mR => mR.UpdateAsync(id, It.IsAny<CompanyModel>()))
                .Throws<CompanyNotFoundDomainException>();
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var point = new CompanyModel()
            {
                Name = "Asus", Description = "In search of incredible"
            };
            
            //Assert
            await Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.UpdateAsync(id, point));

        }

        [Fact]
        public void RemoveCompany_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.DeleteAsync(id))
                .ReturnsAsync(new CompanyModel()
                {
                    CompanyID = id, Name = "Asus", Description = "In search of incredible"
                });
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = service.DeleteAsync(id);

            //Assert
            var result = Assert.IsType<CompanyModel>(check.Result);
            Assert.Equal(id, result.CompanyID);
        }

        [Fact]
        public void RemoveCompany_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.DeleteAsync(id))
                .Throws<CompanyNotFoundDomainException>();
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act & Assert
            Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.DeleteAsync(id));
        }
    }
}