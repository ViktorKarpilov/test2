namespace Company.API.Tests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using API.Controllers;
    using Company.API.Validation;
    using Domain;
    using Domain.Exceptions;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Request;
    using Response;
    using Xunit;

    public class CompanyControllerTests
    {
        [Fact]
        public async void GetAllCompanies_WhenCollectionIsEmpty()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(new List<CompanyModel>());
            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetAllCompanies();

            // Assert
            var list = Assert.IsType<List<CompanyResponse>>(result);
            Assert.Empty(list);
        }

        [Fact]
        public async void GetCompaniesCount_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();
            var testModelsCollection = new ReadOnlyCollection<CompanyModel>(
                new List<CompanyModel>
                {
                    new CompanyModel() { Name = "IBM" },
                    new CompanyModel() { Name = "Apple" },
                    new CompanyModel() { Name = "HP" }
                });
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(testModelsCollection);
            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetAllCompanies();

            // Assert
            var list = Assert.IsType<List<CompanyResponse>>(result);
            Assert.Equal(testModelsCollection.Count, list.Count);
        }

        [Fact]
        public async void GetSingleCompany_WhenItExists()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.GetAsync(requestedId)).ReturnsAsync(
                new CompanyModel()
                {
                    CompanyID = requestedId,
                    Name = "Apple",
                    Description = "Think Different"
                }
            );

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetCompany(requestedId);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(requestedId, response.CompanyID);
        }

        [Fact]
        public async void GetSingleCompany_WhenItDoesNotExist_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.GetAsync(requestedId)).Throws<CompanyNotFoundDomainException>();

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetCompany(requestedId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async void CreateValidCompany()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            var modelToCreate = new CompanyModel()
            {
                Name = "Apple",
                Description = "Think Different"
            };
            Guid generatedId = Guid.NewGuid();
            mockService.Setup(ms => ms.CreateAsync(It.IsAny<CompanyModel>()))
                .ReturnsAsync(() => {
                    modelToCreate.CompanyID = generatedId;
                    return modelToCreate;
                });

            var controller = new CompanyController(mockService.Object);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different"
            };
            var result = await controller.CreateCompany(request);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(generatedId, response.CompanyID);
        }

        [Fact]
        public async void CreateCompany_WhenValidationFailed_CausesBadRequest()
        {
            // Arrange
            var controller = new CompanyController(null);

            // Act
            var request = new CompanyRequest()
            {
                Name = null,
                Description = "Think Different"
            };
            var result = await controller.CreateCompany(request);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async void UpdateCompany_WhenItExists()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<CompanyModel>()))
                .ReturnsAsync(() => new CompanyModel()
                {
                    CompanyID = requestedId,
                    Name = "Apple",
                    Description = "Think Different"
                });
            var controller = new CompanyController(mockService.Object);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different"
            };
            var result = await controller.UpdateCompany(requestedId, request);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(requestedId, response.CompanyID);
            Assert.Equal(request.Name, response.Name);
            Assert.Equal(request.Description, response.Description);
        }

        [Fact]
        public async void UpdateCompany_WhenItDoesNotExist_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<CompanyModel>()))
                .Throws<CompanyNotFoundDomainException>();
            var controller = new CompanyController(mockService.Object);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different"
            };
            var result = await controller.UpdateCompany(requestedId, request);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async void UpdateCompany_WhenValidationFailed_CausesBadRequest()
        {
            // Arrange
            var controller = new CompanyController(null);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = new string('z', 1501)
            };
            var result = await controller.UpdateCompany(Guid.NewGuid(), request);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async void RemoveCompany_WhenItExists()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.DeleteAsync(requestedId)).ReturnsAsync(
                new CompanyModel()
                {
                    CompanyID = requestedId,
                    Name = "Apple",
                    Description = "Think Different"
                }
            );

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.RemoveCompany(requestedId);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(requestedId, response.CompanyID);
        }

        [Fact]
        public async void RemoveCompany_WhenItDoesNotExist_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.DeleteAsync(requestedId)).Throws<CompanyNotFoundDomainException>();

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.RemoveCompany(requestedId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }
    }
}