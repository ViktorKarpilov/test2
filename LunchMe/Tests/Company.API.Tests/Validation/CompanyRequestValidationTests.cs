namespace Company.API.Tests.Validation
{
    using System.Collections.Generic;
    using Company.API.Validation;
    using Request;
    using Xunit;

    public class CompanyRequestValidationTests
    {
        [Theory, MemberData(nameof(TestCases))]
        public void TestCompanyRequestValidation(CompanyRequest request, bool expectedResult)
        {
            // Arrange
            var crv = new CompanyRequestValidator();
            // Act
            bool actualResult = crv.Validate(request).IsValid;
            // Assert
            Assert.Equal(expectedResult, actualResult);
        }

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = "Apple",
                        Description = "Think Different"
                    }, true
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = null,
                        Description = "Think Different"
                    }, false
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = "",
                        Description = "Think Different"
                    }, false
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = new string('z', 1501),
                        Description = "Think Different"
                    }, false
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = "Apple",
                        Description = new string('z', 1501)
                    }, false
                };
            }
        }
    }
}