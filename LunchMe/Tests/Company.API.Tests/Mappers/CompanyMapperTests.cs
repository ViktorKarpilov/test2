namespace Company.API.Tests.Mappers
{
    using System;
    using System.Collections.Generic;
    using API.Mappers;
    using Domain;
    using Request;
    using Xunit;

    public class CompanyMapperTests
    {
        [Theory]
        [InlineData("Apple", "Think Different")]
        [InlineData(null, "Think Different")]
        [InlineData("Apple", "")]
        public void TestMappingToModelFromRequest(string name, string description)
        {
            // Arrange
            var request = new CompanyRequest()
            {
                Name = name,
                Description = description
            };
            // Act
            var model = request.MapFromRequest();
            // Assert
            Assert.Equal(request.Name, model.Name);
            Assert.Equal(request.Description, model.Description);
        }

        [Theory, MemberData(nameof(TestCases))]
        public void TestMappingFromModelToResponse(Guid id, string name, string description)
        {
            // Arrange
            var model = new CompanyModel()
            {
                CompanyID = id,
                Name = name,
                Description = description
            };
            // Act
            var response = model.MapToResponse();
            // Assert
            Assert.Equal(model.CompanyID, response.CompanyID);
            Assert.Equal(model.Name, response.Name);
            Assert.Equal(model.Description, response.Description);
        }

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                yield return new object[] {Guid.NewGuid(), "Apple", "Think Different"};
                yield return new object[] {null, "Apple", "Think Different"};
                yield return new object[] {Guid.NewGuid(), "", "Think Different"};
                yield return new object[] {Guid.NewGuid(), "Apple", null};
            }
        }
    }
}